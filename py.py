import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import random


class Point(object):
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def __str__(self):
        return f"({self.x},{self.y})"


class Goal(object):
    def __init__(self):
        self.points = list()

    def __str__(self):
        return ", ".join([str(_) for _ in self.points])

    def add_point(self, p: Point):
        self.points.append(p)


class Poly(object):
    def __init__(self, coefs: list):
        self.coefs = coefs
        self.error = None

    def __str__(self):
        return ", ".join([str(_) for _ in self.coefs])


def grade(goal: Goal, poly: Poly):
    error = 0  # lower == better
    for p in goal.points:
        # quadratic error should be more efficient, and >= 0
        error += (p.y - np.polyval(poly.coefs, p.x)) ** 2
    return error


def create_mutant(poly: Poly, round_idx: int):
    return Poly(
        [
            coef + random.uniform(-0.2, 0.2) * (round_idx / NB_ROUND) * POP_RANGE
            for coef in poly.coefs
        ]
    )


def plotStuff(goal: Goal, folks: list, title: str):
    global axis
    plt.clf()
    plt.plot()
    plt.title(title)
    plt.xlabel("x")
    plt.ylabel("y")
    plt.grid()
    t = np.arange(-2.0, 2.0, 0.01)

    for folk in folks:
        s = np.polyval(folk.coefs, t)
        foo = plt.plot(t, s)
        del foo
    plt.legend([f"{int(folk.error)}" for folk in folks])

    for p in goal.points:
        plt.plot(p.x, p.y, "ro")

    if not axis:
        axis = plt.axis()
    else:
        plt.axis(axis)
    plt.savefig(title)
    # plt.show()


POP_SIZE = 20
POP_COEFS = 5
POP_RANGE = 10
NB_ROUND = 100
goal = Goal()
axis = None

for x in range(-2, 3, 1):
    goal.add_point(Point(x, random.randint(-0.5 * POP_RANGE, 0.5 * POP_RANGE)))


folks = list()
for i in range(POP_SIZE):
    folks.append(
        Poly([random.randint(-POP_RANGE, POP_RANGE) for _ in range(POP_COEFS)])
    )

for round_idx in range(1, NB_ROUND + 1):
    print(f" Round {round_idx} ".center(40, "*"))
    # Grade & Sort
    for folk in folks:
        folk.error = grade(goal, folk)
    folks.sort(key=lambda f: f.error)

    # Plot
    plotStuff(goal, folks, f"round-{round_idx:03}")

    # Keep the bests and mutate
    bests = folks[: round(POP_SIZE / 5)]
    folks = folks[: round(POP_SIZE / 5)]

    for best in bests:
        for i in range(4):
            folks.append(create_mutant(best, round_idx))

    print(f"Population   : {len(folks)}")
    print(f"Current best : {folks[0].error}")

plotStuff(goal, bests, "round-final")
