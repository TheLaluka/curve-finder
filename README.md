# curve-finder

Approximate a best fit for multiple points placed on a graph using genetic (extra-lite) evolution. 

1. Specify a few parameters (round count, goal, coef count, variation range, ...)
1. Run it
1. Watch a cool git
1. Write more shitty code ! :)


# Whole process

![Whole process](screens/evolution.gif)


# first round

![first round](screens/first.png)


# final round

![final round](screens/final.png)
